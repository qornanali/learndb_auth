/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package learndb_auth;

import java.sql.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author qornanali
 */
public class MysqlHelper {
    
     static Connection connect = null;
    static Statement statement = null;
    PreparedStatement preparedStatement = null;
    ResultSet resultSet = null;
    
     // JDBC driver name and database URL
   static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
   static final String DB_URL = "jdbc:mysql://localhost/jtk";

   //  Database credentials
   static final String USER = "root";
   static final String PASS = "";

    public MysqlHelper() {
         try {
            Class.forName("com.mysql.jdbc.Driver");
            connect = DriverManager.getConnection(DB_URL,USER,PASS);
            statement = connect.createStatement();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(MysqlHelper.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(MysqlHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public ArrayList<Auth> getAuths(String where){
        ResultSet resultSet = null;
        ArrayList<Auth> auths = new ArrayList<>();
        try {
            String sql = "SELECT * FROM auth " + where + ";";
            System.out.println(sql);
            resultSet = statement.executeQuery(sql);   
            while(resultSet.next()){
                auths.add(new Auth(resultSet.getString(1), resultSet.getString(2), resultSet.getString(3)));
            }
            resultSet.close();
        } catch (SQLException ex) {
             Logger.getLogger(MysqlHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
        return auths;
    }
    
    public void addNewAuth(Auth newauth){
         try {
            String sql = "INSERT INTO auth "
                     + "VALUES (NULL,'"+newauth.getEmail()+"',md5('"+newauth.getPassword()+"'));";
            System.out.println(sql);
            statement.execute(sql);
         } catch (SQLException ex) {
             Logger.getLogger(MysqlHelper.class.getName()).log(Level.SEVERE, null, ex);
         }
    }
    
    public void updateAuth(Auth oldauth, Auth newauth){
        try { 
            String sql = "UPDATE auth SET email = '"+newauth.getEmail()+"'"
                     + ", password = md5('"+newauth.getPassword()+"') WHERE "
                     + "email = '"+oldauth.getEmail()+"';";
            System.out.println(sql);
            statement.execute(sql);
         } catch (SQLException ex) {
             Logger.getLogger(MysqlHelper.class.getName()).log(Level.SEVERE, null, ex);
         }
    }
    
    public void deleteAuth(Auth oldauth){
        try { 
            String sql = "DELETE FROM auth WHERE email = '"+oldauth.getEmail()+"'"
                     + " AND password = '"+oldauth.getPassword()+"';";
            System.out.println(sql);
            statement.execute(sql);
         } catch (SQLException ex) {
             Logger.getLogger(MysqlHelper.class.getName()).log(Level.SEVERE, null, ex);
         }
    }
}
