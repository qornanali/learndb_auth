/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package learndb_auth;

/**
 *
 * @author qornanali
 */
public class Auth {
    
    private String kd_auth,email,password;

    public Auth() {
    }

    public Auth(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public Auth(String kd_auth, String email, String password) {
        this.kd_auth = kd_auth;
        this.email = email;
        this.password = password;
    }
    

    public String getKd_auth() {
        return kd_auth;
    }

    public void setKd_auth(String kd_auth) {
        this.kd_auth = kd_auth;
    }
    
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    
    
}
